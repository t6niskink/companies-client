
// Functions for communicating with backend-API here...

function fetchCompanies() {
    return fetch(`${API_URL}/companies`, { method: 'GET' })
        .then(response => response.json());
}

function fetchCompany(id) {
    return fetch(
        `${API_URL}/company?id=${id}`, { method: 'GET' })
        .then(response => response.json());
}


function deleteCompany(id) {
    return fetch(
        `${API_URL}/company?id=${id}`, { method: 'DELETE' });
}

//Company changing
function putCompany(company) {
    return fetch(
        `${API_URL}/company`,
        {method: 'PUT',
        headers: {"Content-Type": "application/json"},
        body: JSON.stringify(company)});
}


// Company adding
function postCompany(company) {
    return fetch(
        `${API_URL}/company`,
        {method: 'POST',
        headers: {"Content-Type": "application/json"},
        body: JSON.stringify(company)});
}





function postSomething(something) {
            return fetch(
                `${API_URL}/something`,
                {
                    method: 'POST',
                    body: JSON.stringify(something)
                }
            );
        }

function deleteSomething(id) {
            return fetch(
                `${API_URL}/something/${id}`,
                {
                    method: 'DELETE'
                }
            );
        }
